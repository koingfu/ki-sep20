# Ki SEP20 Token

Ki SEP20 contract address: 0x4524cE998c2551CdA6d5763E0AD74153059B6207

## $Ki
- max supply: 210,000,000,000
- Is not mintable.
- Is burnable.

Compiled with Openzepellin release V4.5.0
https://github.com/OpenZeppelin/openzeppelin-contracts/releases/tag/v4.5.0

Solidity 0.8.12, Optimizer enable 1000000 runs.
