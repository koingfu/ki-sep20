///SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.12;

import "../openzeppelin/contracts/token/ERC20/ERC20.sol";
import "../openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";

contract MyToken is ERC20, ERC20Burnable {
    constructor() ERC20("Ki", "Ki") {
        _mint(msg.sender, 210000000000 * 10 ** decimals());
    }
}
